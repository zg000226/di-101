"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var config = require("config");
var common_1 = require("@nestjs/common");
var Config = /** @class */ (function () {
    function Config() {
        this._config = config.util.toObject(); // simply get config as object
        console.log('Config Provider initialized');
        console.log(this._config);
    }
    Config.prototype.getAllConfig = function () {
        return JSON.parse(JSON.stringify(this._config)); // cast config to be immutable
    };
    Config = __decorate([
        common_1.Injectable() // This class is a provider!
    ], Config);
    return Config;
}());
exports.Config = Config;
