export interface IBook {
    getISBN(): string
    getAuthor(): string
}
