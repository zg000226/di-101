import { NestFactory } from '@nestjs/core'
import {
    Module,
    Provider,
} from '@nestjs/common'
import * as config from 'config'
import { mongoConnectionProvider } from './provider/mongo-connection'
import { IndexController } from './index.controller'
import { LibraryRepository } from './repository/library'

// define configProvider
export const configProvider: Provider = {
    provide: 'ENV_CONFIG', // named this provider
    useValue: config.util.toObject(),
}

@Module({
    controllers: [
        IndexController,
    ],
    providers: [
        configProvider, // register ENV_CONFIG
        mongoConnectionProvider, // register MONGO_CONNECTION
        LibraryRepository,
    ],
})
class AppModule {
}

async function bootstrap() {
    const app = await NestFactory.create(AppModule)
    await app.listen(3000)
}

bootstrap()
