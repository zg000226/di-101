import { ILibraryRepository } from '../interface/repository/library.interface'
import {
    Collection,
    Db,
} from 'mongodb'
import { IBook } from '../interface/model/book.interface'
import {
    Inject,
    Injectable,
} from '@nestjs/common'

@Injectable()
export class LibraryRepository implements ILibraryRepository {
    private _collection: Collection

    constructor(
        @Inject('MONGO_CONNECTION')
        private readonly _db: Db,
    ) {
        // set mongo collection for this repository
        this._collection = this._db.collection('library')
    }

    public getByISBN(isbn: string): Promise<IBook> {
        const query = {
            _id: isbn,
        }
        return this._collection.findOne(query)
    }
}
